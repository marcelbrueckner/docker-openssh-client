# openssh-client

A Docker image with OpenSSH for use with GitLab CI.

```yaml
image: registry.gitlab.com/marcelbrueckner/docker-openssh-client:latest
```
